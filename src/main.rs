use std::convert::TryFrom;

fn main() {
    // use kernel::math_struct::numbers::rational::Rational;
    // let fraction = Rational::from(std::f64::consts::PI);
    // println!("pi = {:#?}", fraction);
    // println!("{:?}", fraction.0.dividers());
    // println!("{:?}", fraction.1.dividers());
    // let fraction = Rational::from(std::f64::consts::E);
    // println!("e = {:#?}", fraction);
    // println!("{:?}", fraction.0.dividers());
    // println!("{:?}", fraction.1.dividers());

    use kernel::math_struct::number::VeryLargeNumber;

    let mut vln1: VeryLargeNumber = 1.into();
    vln1 += dbg!(dbg!(VeryLargeNumber::from(u128::MAX)) << 4);
    println!("vln1 :\n{:#?}", vln1);
    dbg!(VeryLargeNumber::from(2));
    dbg!(VeryLargeNumber::from(3));
    dbg!(VeryLargeNumber::from(4));
    dbg!(VeryLargeNumber::from(5));
    dbg!(VeryLargeNumber::from(u64::MAX));
}


//  0 1 0 0 1 0 0 1 0 0 0 
//  1 2 4 8 6 2 4 8 6 2 4 % 10 
//  0 0 0 0 1 3 6 2 5 1 2 % 10
//  0 0 0 0 0 0 0 1 2 5 0 % 10
//  0 0 0 0 0 0 0 0 0 0 1 % 10
//
//  146