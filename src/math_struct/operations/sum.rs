
use super::super::number::Number;

use std::ops::{Deref, DerefMut};

pub struct Sum(pub Vec<Box<dyn Number>>);

impl Sum {
    pub fn new() -> Self {
        Sum(Vec::new())
    }
}

impl Deref for Sum {
    type Target = Vec<Box<dyn Number>>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Sum {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Number for Sum {
    fn to_numeric(&self) -> f64 {
        let mut sum = 0_f64;
        for value in self.iter() {
            sum += value.to_numeric();
        }
        sum
    }
}