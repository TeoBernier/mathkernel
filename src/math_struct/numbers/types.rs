pub type Scalar = f64;
pub type IntScalar = i64;