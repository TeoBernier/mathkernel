use super::integer::Integer;
use super::types::{Scalar, IntScalar};
use super::super::number::Number;

#[derive(Clone, Copy, Debug)]
pub struct Rational(pub Integer, pub Integer);

impl Rational {
    pub fn simplify(&mut self) {
        let pgcd = self.0.pgcd(self.1);
        self.0.0 /= pgcd.0;
        self.1.0 /= pgcd.0;
    }
}

impl Number for Rational {
    fn to_numeric(&self) -> Scalar {
        self.0.to_numeric() / self.1.to_numeric()
    }
}

use std::convert::From;

impl From<Integer> for Rational {
    fn from(integer: Integer) -> Rational {
        Rational(integer, Integer::from(1 as IntScalar))
    }
}

impl From<IntScalar> for Rational {
    fn from(number: IntScalar) -> Rational {
        Rational(Integer::from(number), Integer::from(1 as IntScalar))
    }
}

impl From<Scalar> for Rational {
    fn from(mut number: Scalar) -> Rational {
        let mut denominator = 1;
        while number.fract() > 0_f64 {
            number *= 2_f64;
            denominator *= 2;
        }
        Rational(Integer::from(number), Integer::from(denominator))
    }
}

use std::ops::{Add, Sub, Mul, Div};

//########################### Behavior with Integers #######################

impl Add<Integer> for Rational {
    type Output = Self;

    fn add(self, other: Integer) -> Self::Output {
        other + self
    }
}

impl Sub<Integer> for Rational {
    type Output = Self;

    fn sub(self, other: Integer) -> Self::Output {
        Rational(self.0 - other * self.1, self.1)
    }
}

impl Mul<Integer> for Rational {
    type Output = Self;

    fn mul(self, other: Integer) -> Self::Output {
        other * self
    }
}

impl Div<Integer> for Rational {
    type Output = Rational;

    fn div(self, other: Integer) -> Self::Output {
        Rational(self.0, self.1 * other)
    }
}

//########################### Behavior with Rationals #######################

impl Add<Rational> for Rational {
    type Output = Rational;

    fn add(self, other: Rational) -> Self::Output {
        Rational(self.0 * other.1 + other.0 * self.1, self.1 * other.1)
    }
}

impl Sub<Rational> for Rational {
    type Output = Rational;

    fn sub(self, other: Rational) -> Self::Output {
        Rational(self.0 * other.1 - other.0 * self.1, self.1 * other.1)
    }
}

impl Mul<Rational> for Rational {
    type Output = Rational;

    fn mul(self, other: Rational) -> Self::Output {
        Rational(self.0 * other.0, self.1 * other.1)
    }
}

impl Div<Rational> for Rational {
    type Output = Rational;

    fn div(self, other: Rational) -> Self::Output {
        Rational(self.0 * other.1, self.1 * other.0)
    }
}