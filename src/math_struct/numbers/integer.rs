use super::types::*;
use super::super::number::Number;
use super::rational::Rational;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct Integer(pub IntScalar);

//boucle d'incrementation prennat en compte les multiples des premiers (2, 3, 5 et 7)
const BEGIN_PRIME_CYCLE: [IntScalar; 4] = [1, 2, 2, 4];
const PRIME_CYCLE: [IntScalar; 48] = [2, 4, 2, 4, 6, 2, 6, 4, 2, 4, 6, 6, 2, 6, 4, 2, 6, 4, 6, 8, 4, 2, 4, 2, 4, 8, 6, 4, 6, 2, 4, 6, 2, 6, 6, 4, 2, 4, 6, 2, 6, 4, 2, 4, 2, 10, 2, 10];

impl Integer {
    pub fn dividers(&self) -> Vec<(IntScalar, IntScalar)> {
        if self.0 <= 1 {
            Vec::new()
        } else {
            let mut number_divided = self.0;
            let mut dividers = Vec::new();
            let mut primes: IntScalar = 2;
            let mut limit = (number_divided as f64).sqrt() as IntScalar;
            for step in BEGIN_PRIME_CYCLE.iter().chain(PRIME_CYCLE.iter().cycle()) {
                if primes > limit {
                    break;
                } else {
                    if number_divided % primes == 0 {
                        let mut count = 1;
                        number_divided /= primes;
                        while number_divided % primes == 0 {
                            count += 1;
                            number_divided /= primes;
                        }
                        limit = (number_divided as f64).sqrt() as IntScalar;
                        dividers.push((count, primes));
                    }
                }
                primes += step;
            }
            if number_divided > 1 {
                dividers.push((1, number_divided));
            }
            dividers
        }
    }

    pub fn pgcd(&self, other: Integer) -> Integer {
        let self_divs = self.dividers();
        let other_divs = other.dividers();
        let mut self_divs_iter = self_divs.iter();
        let mut other_divs_iter = other_divs.iter();
        let mut self_div = self_divs_iter.next();
        let mut other_div = other_divs_iter.next();
        if self_div == None || other_div == None {
            return Integer::from(1);
        }
        let mut pgcd = 1;
        let mut self_divider = self_div.unwrap();
        let mut other_divider = other_div.unwrap();
        loop {
            if self_divider.1 == other_divider.1 {
                pgcd *= self_divider.1.pow(self_divider.0.min(other_divider.0) as u32);
                self_div = self_divs_iter.next();
                other_div = other_divs_iter.next(); 
                if self_div != None && other_div != None {
                    self_divider = self_div.unwrap();
                    other_divider = other_div.unwrap();
                } else {
                    break
                }
            } else if self_divider < other_divider {
                self_div = self_divs_iter.next();
                if self_div != None {
                    self_divider = self_div.unwrap();
                } else {
                    break
                }
            } else {
                other_div = other_divs_iter.next();
                if other_div != None {
                    other_divider = other_div.unwrap();
                } else {
                    break
                }
            }
        }
        Integer::from(pgcd)
    }

}

impl Number for Integer {
    fn to_numeric(&self) -> Scalar {
        self.0 as Scalar
    }
}

use std::convert::From;

impl From<Scalar> for Integer {
    fn from(number: Scalar) -> Self {
        Integer(number as IntScalar)
    }
}

impl From<IntScalar> for Integer {
    fn from(number: IntScalar) -> Self {
        Integer(number)
    }
}

use std::ops::{Add, Mul, Sub, Div};

//########################### Behavior with Integers #######################

impl Add<Integer> for Integer {
    type Output = Self;

    fn add(self, other: Integer) -> Self::Output {
        Integer(self.0 + other.0)
    }
}

impl Sub<Integer> for Integer {
    type Output = Self;

    fn sub(self, other: Integer) -> Self::Output {
        Integer(self.0 - other.0)
    }
}

impl Mul<Integer> for Integer {
    type Output = Self;

    fn mul(self, other: Integer) -> Self::Output {
        Integer(self.0 * other.0)
    }
}

impl Div<Integer> for Integer {
    type Output = Rational;

    fn div(self, other: Integer) -> Self::Output {
        Rational(self, other)
    }
}

//########################### Behavior with Rationals #######################

impl Add<Rational> for Integer {
    type Output = Rational;

    fn add(self, other: Rational) -> Self::Output {
        Rational(other.0 + (self * other.1), other.1)
    }
}

impl Sub<Rational> for Integer {
    type Output = Rational;

    fn sub(self, other: Rational) -> Self::Output {
        Rational((self * other.1) - other.0, other.1)
    }
}

impl Mul<Rational> for Integer {
    type Output = Rational;

    fn mul(self, other: Rational) -> Self::Output {
        Rational(self * other.0, other.1)
    }
}

impl Div<Rational> for Integer {
    type Output = Rational;

    fn div(self, other: Rational) -> Self::Output {
        Rational(self * other.1, other.0)
    }
}