use super::operations::sum::Sum;
use std::convert::{From, TryFrom, TryInto};
use std::fmt::Debug;
use std::ops::Shl;
use std::ops::Shr;
use std::ops::{AddAssign, DivAssign, MulAssign, ShlAssign, ShrAssign, SubAssign};

type Fragment = u128;

pub trait Number {
    fn to_numeric(&self) -> f64;
}

#[derive(Debug)]
pub struct VeryLargeNumber {
    bits_used: usize,
    decalage: usize,
    digits: Vec<Fragment>,
}

impl VeryLargeNumber {
    pub fn new(number: impl Into<Self>) -> Self {
        number.into()
    }

    pub fn with_capacity(bits_needed: usize) -> Self {
        Self {
            bits_used: bits_needed,
            decalage: 0,
            digits: Vec::with_capacity((bits_needed + 127) / 128),
        }
    }

    pub fn used_length(&self) -> usize {
        self.bits_used + self.decalage
    }

    pub fn total_length(&self) -> usize {
        self.decalage + self.digits.len() * 128
    }

    pub fn add_assign_fragment(frag1: &mut Fragment, frag2: &Fragment, overflow: bool) -> bool {
        let result = frag1.overflowing_add(*frag2);
        if result.1 {
            if overflow {
                *frag1 = result.0 + 1;
            } else {
                *frag1 = result.0;
            }
            true
        } else {
            if overflow {
                if result.0 == u128::MAX {
                    *frag1 = 0;
                    true
                } else {
                    *frag1 = result.0 + 1;
                    false
                }
            } else {
                *frag1 = result.0;
                false
            }
        }
    }

    pub fn add_assign_fragment_array(
        digits1: &mut [u128],
        digits2: &[u128],
        mut overflow: bool,
    ) -> bool {
        for (frag1, frag2) in digits1.iter_mut().zip(digits2.iter()) {
            overflow = Self::add_assign_fragment(frag1, frag2, overflow);
        }
        return overflow;
    }

    pub fn add_assign_fragment_array_shifted(
        digits1: &mut [u128],
        digits2: &[u128],
        mut overflow: bool,
        shifted_difference: u64,
    ) -> bool {
        if shifted_difference == 0 {
            Self::add_assign_fragment_array(digits1, digits2, overflow)
        } else {
            let mut old_frag: &u128 = &0_u128;

            let begin_mask: u128 = (1_u128 << (128 - shifted_difference)) - 1;
            let begin_shift: u64 = shifted_difference;
            let end_mask: u128 = !begin_mask;
            let end_shift: u64 = 128 - begin_shift;

            for (frag1, frag2) in digits1
                .iter_mut()
                .zip(digits2.iter().chain([0_u128].iter()))
            {
                let frag_tmp =
                    ((frag2 & begin_mask) << begin_shift) + ((old_frag & end_mask) >> end_shift);
                overflow = Self::add_assign_fragment(frag1, &frag_tmp, overflow);
                old_frag = frag2;
            }
            overflow
        }
    }
}

use duplicate::duplicate;
#[duplicate(
    NUM implementation;
    #[ type_nested; [i8]; [i16]; [i32]; [i64]; [i128]; [isize] ]
    [[ type_nested ] [ if number < 0 { panic!("Cannot convert a negative number to a VeryLargeNumber."); } ];]
    #[type_nested; [u8]; [u16]; [u32]; [u64]; [u128]; [usize] ]
    [[ type_nested ]  [ ];]
 )]
impl From<NUM> for VeryLargeNumber {
    fn from(mut number: NUM) -> Self {
        use std::mem::size_of;
        implementation;
        if number <= 1 {
            Self {
                bits_used: number as usize,
                decalage: 0,
                digits: vec![number as u128],
            }
        } else {
            let exponent = size_of::<NUM>() * 8 - number.leading_zeros() as usize;
            let decalage = number.trailing_zeros() as usize;
            number >>= decalage;

            Self {
                bits_used: exponent - decalage,
                decalage,
                digits: vec![number as u128],
            }
        }
    }
}

/*
impl<E: Debug, T: Sized + TryInto<Fragment, Error = E>> From<T> for VeryLargeNumber {
    fn from(number: T) -> Self {
        let mut number = number.try_into().unwrap();
        if number <= 1 {
            Self {
                bits_used: number as usize,
                decalage: 0,
                digits: vec![number],
            }
        } else {
            let exponent = 128 - number.leading_zeros() as usize;
            let decalage = number.trailing_zeros() as usize;
            number >>= decalage;

            Self {
                bits_used: exponent - decalage,
                decalage,
                digits: vec![number],
            }
        }
    }
}
*/

/*
    Fragment = 0000;
    nb1 = 0010_1100_0001 << 10

    nb2 =  001_1111_1 << 14
        =   0011_1111 << 14

    end(frag)   = frag & 0b1110 >> 1
    begin(frag) = frag & 0b0001 << 3

    nb3 = ????_????_?001 << 10

*/

use std::mem;

impl AddAssign for VeryLargeNumber {
    fn add_assign(&mut self, mut rhs: Self) {
        if self.decalage != rhs.decalage {
            if self.decalage > rhs.decalage {
                mem::swap(self, &mut rhs);
            }

            if self.total_length() < rhs.used_length() {
                self.digits.resize_with(
                    self.digits.len() + (rhs.used_length() - self.total_length() + 127) / 128,
                    u128::default,
                );
            }

            let diff = rhs.decalage - self.decalage;
            let shifted_difference = diff % 128;
            let check_overflow = Self::add_assign_fragment_array_shifted(
                &mut self.digits[(diff / 128)..],
                &rhs.digits,
                false,
                (shifted_difference) as u64,
            );

            self.bits_used = rhs.used_length() - self.decalage;

            if check_overflow {
                self.bits_used += 1;
                self.digits.push(1_u128);
            }
        } else {
        }
    }
}

impl Default for VeryLargeNumber {
    fn default() -> Self {
        Self::new(0)
    }
}

impl<T: Into<i64>> ShlAssign<T> for VeryLargeNumber {
    fn shl_assign(&mut self, rhs: T) {
        let number = rhs.into();
        if number >= 0 {
            self.decalage += number as usize;
        } else {
            *self >>= number.abs();
        }
    }
}

impl<T: Into<i64>> Shl<T> for VeryLargeNumber {
    type Output = Self;

    fn shl(mut self, rhs: T) -> Self {
        self <<= rhs;
        self
    }
}

impl<T: Into<i64>> ShrAssign<T> for VeryLargeNumber {
    fn shr_assign(&mut self, rhs: T) {
        let number = rhs.into();
        if number >= 0 {
            self.decalage -= number as usize;
        } else {
            *self <<= number.abs();
        }
    }
}

impl<T: Into<i64>> Shr<T> for VeryLargeNumber {
    type Output = Self;

    fn shr(mut self, rhs: T) -> Self {
        self >>= rhs;
        self
    }
}
